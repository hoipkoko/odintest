using MXM_Script;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SubAssetList", menuName = "SubAssetList Asset/Create SubAssetList", order = 1)]
public class SubAssetList : SerializedScriptableObject
{
	[LabelText("���dID")]
	public List<string> StageName;

	//public SubAssetDictionary _subAssetDictionary;
}
