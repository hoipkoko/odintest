using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine;
using System.Collections.Generic;

namespace MXM_Script
{
	[System.Serializable]
	public class BundleLoaderDictionary : UnitySerializedDictionary<string, string> { }

	public class BundleLoader : MonoBehaviour
	{
		[SerializeField]
		private BundleLoaderDictionary _bundle;

		ProductList _productList;

		public void LoadStage(string _key)
		{
			LoadStageAsync(_key);
		}

		private async UniTask LoadStageAsync(string _key)
		{
			_productList = await Addressables.LoadAssetAsync<ProductList>(_bundle[_key]);

			if (_productList != null)
				for (int i = 0; i < _productList._pList.Count; i++)
				{
					Debug.Log(_productList._pList[i].Name);
					Debug.Log(_productList._pList[i].Description);
					Debug.Log(_productList._pList[i].Price);
				}
		}
	}
}