using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

namespace MXM_Script
{
	public class MXMDotween : MonoBehaviour
	{
		public static MXMDotween instance;

		Sequence Tween;

		private void Awake()
		{
			instance = this;
		}

		public void Fade(CanvasGroup _cg, bool _state, float _time, float _alpha = 0, Ease _easeType = Ease.Linear, UnityAction _startAc = null, UnityAction _endAc = null)
		{
			FadeInternal(_cg, _state, _time, _alpha, _easeType, _startAc, _endAc);
		}

		public void Scale(Transform _trans, bool _state, float _time, Ease _easeType = Ease.Linear, UnityAction _startAc = null, UnityAction _endAc = null)
		{
			ScaleInternal(_trans, _state, _time, _easeType, _startAc, _endAc);
		}

		public void AncPos(RectTransform _trans, Vector2 _pos, float _time, Ease _easeType = Ease.Linear, UnityAction _startAc = null, UnityAction _endAc = null)
		{
			AncPosInternal(_trans, _pos, _time, _easeType, _startAc, _endAc);
		}

		public void AncSize(RectTransform _trans, Vector2 _size, float _time, Ease _easeType = Ease.Linear, UnityAction _startAc = null, UnityAction _endAc = null)
		{
			AncSizeInternal(_trans, _size, _time, _easeType, _startAc, _endAc);
		}

		private void FadeInternal(CanvasGroup _cg, bool _state, float _time, float _alpha = 0, Ease _easeType = Ease.Linear, UnityAction _startAc = null, UnityAction _endAc = null)
		{
			Tween = DOTween.Sequence();

			Tween.Join(_cg.DOFade(_state ? 1 : _alpha, _time).SetEase(_easeType).OnStart(() =>
			{
				_cg.blocksRaycasts = _state;
				_startAc.Invoke();
			}).OnComplete(() =>
			{
				_endAc.Invoke();
			}));
		}

		private void ScaleInternal(Transform _trans, bool _state, float _time, Ease _easeType = Ease.Linear, UnityAction _startAc = null, UnityAction _endAc = null)
		{
			Tween = DOTween.Sequence();

			Tween.Join(_trans.DOScale(_state ? Vector3.one : Vector3.zero, _time).SetEase(_easeType).OnStart(() =>
			{
				_startAc.Invoke();
			}).OnComplete(() =>
			{
				_endAc.Invoke();
			}));
		}

		private void AncPosInternal(RectTransform _trans, Vector2 _pos, float _time, Ease _easeType = Ease.Linear, UnityAction _startAc = null, UnityAction _endAc = null)
		{
			Tween = DOTween.Sequence();

			Tween.Join(_trans.DOAnchorPos(_pos, _time).SetEase(_easeType).OnStart(() =>
			{
				_startAc.Invoke();
			}).OnComplete(() =>
			{
				_endAc.Invoke();
			}));
		}

		private void AncSizeInternal(RectTransform _trans, Vector2 _size, float _time, Ease _easeType = Ease.Linear, UnityAction _startAc = null, UnityAction _endAc = null)
		{
			Tween = DOTween.Sequence();

			Tween.Join(_trans.DOSizeDelta(_size, _time).SetEase(_easeType).OnStart(() =>
			{
				_startAc.Invoke();
			}).OnComplete(() =>
			{
				_endAc.Invoke();
			}));
		}
	}
}
