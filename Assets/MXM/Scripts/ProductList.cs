using Sirenix.OdinInspector;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

[CreateAssetMenu(fileName = "New ProductList", menuName = "ProductList Asset/Create ProductList", order = 1)]
public class ProductList : SerializedScriptableObject
{
	[TableList]
	public List<Product> _pList;
	//[BoxGroup("Product")]
	//[HorizontalGroup("Product/Split", 120, MarginLeft = 10, MarginRight = 10, LabelWidth = 200)]
	//[VerticalGroup("Product/Split/Left", PaddingTop = 10, PaddingBottom = 10)]
	//[HideLabel, PreviewField(120)]
	//public Texture ProductImage;

	//[HorizontalGroup("Product/Split/Right", MarginRight = 10)]
	//[VerticalGroup("Product/Split/Right/Split", PaddingTop = 10)]
	//[LabelText("商品編號"), LabelWidth(60)]
	//public string ID;

	//[Space]
	//[VerticalGroup("Product/Split/Right/Split")]
	//[LabelText("商品名稱"), LabelWidth(60)]
	//public string Name;

	//[Space]
	//[VerticalGroup("Product/Split/Right/Split")]
	//[LabelText("商品價格"), LabelWidth(60)]
	//public string Price;
}

[System.Serializable]
public class Product
{
	[PreviewField(147)]
	[TableColumnWidth(153, Resizable = false)]
	public Texture Icon;

	[TextArea(10, 10)]
	public string Description;

	[HorizontalGroup("Split", MarginRight = 12, MarginLeft = 15)]
	[VerticalGroup("Split/Information", PaddingTop = 22), LabelWidth(50)]
	[LabelText("編號")]
	public string ID;

	[PropertySpace(22)]
	[VerticalGroup("Split/Information"), LabelWidth(50)]
	[LabelText("名稱")]
	public string Name;

	[PropertySpace(22)]
	[VerticalGroup("Split/Information"), LabelWidth(50)]
	[LabelText("價格")]
	public string Price;

}
