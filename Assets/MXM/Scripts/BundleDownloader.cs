using UnityEngine;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Linq;

namespace MXM_Script
{
	[System.Serializable]
	public class SubAssetDictionary
	{
		public string SubAssetName;

		public List<AssetReference> _subAssetReference = new List<AssetReference>();
	}

	public class BundleDownloader : MonoBehaviour
	{
		[BoxGroup("Bundle Downloader")]
		[VerticalGroup("Bundle Downloader/1", PaddingTop = 6, PaddingBottom = 6)]
		[HorizontalGroup("Bundle Downloader/1/1", MarginLeft = 10, MarginRight = 10, LabelWidth = 100)]
		[VerticalGroup("Bundle Downloader/1/1/1", PaddingTop = 5, PaddingBottom = 5)]
		[SerializeField]
		[LabelText("主要資源")]
		private List<AssetReference> _mainAssetReference = new List<AssetReference>();

		[HorizontalGroup("Bundle Downloader/1/2", MarginLeft = 10, MarginRight = 10, LabelWidth = 100)]
		[VerticalGroup("Bundle Downloader/1/2/1", PaddingTop = 5, PaddingBottom = 5)]
		[SerializeField]
		[LabelText("次要資源清單")]
		private List<SubAssetDictionary> _subAssetList;

		//[HorizontalGroup("Bundle Downloader/1/3", MarginLeft = 10, MarginRight = 10, LabelWidth = 100)]
		//[VerticalGroup("Bundle Downloader/1/3/1", PaddingTop = 5, PaddingBottom = 5)]
		//[SerializeField]
		//[LabelText("次要資源清單路徑")]
		//private string SubAssetListPath = "Assets/MXM/Addressable/Nes/StageList.asset";

		[HorizontalGroup("Bundle Downloader/1/4", MarginLeft = 10, MarginRight = 10, LabelWidth = 100)]
		[VerticalGroup("Bundle Downloader/1/4/1", PaddingTop = 2, PaddingBottom = 2)]
		[SerializeField]
		[LabelText("下載資訊")]
		private bool IsShowMsg = true;

		[HorizontalGroup("Bundle Downloader/1/5", MarginLeft = 10, MarginRight = 10, LabelWidth = 100)]
		[VerticalGroup("Bundle Downloader/1/5/1", PaddingTop = 2, PaddingBottom = 2)]
		[SerializeField]
		[LabelText("資訊文字")]
		private PopText _popText;

		[HorizontalGroup("Bundle Downloader/1/6", MarginLeft = 10, MarginRight = 10, LabelWidth = 100)]
		[VerticalGroup("Bundle Downloader/1/6/1", PaddingTop = 2, PaddingBottom = 2)]
		[SerializeField]
		[LabelText("下載提示CG")]
		private CanvasGroup DownloadFrameCG;

		[HorizontalGroup("Bundle Downloader/1/7", MarginLeft = 10, MarginRight = 10, LabelWidth = 100)]
		[VerticalGroup("Bundle Downloader/1/7/1", PaddingTop = 2, PaddingBottom = 2)]
		[SerializeField]
		[LabelText("下載提示Rect")]
		private RectTransform DownloadFrameRect;

		[HorizontalGroup("Bundle Downloader/1/8", MarginLeft = 10, MarginRight = 10, LabelWidth = 100)]
		[VerticalGroup("Bundle Downloader/1/8/1", PaddingTop = 2, PaddingBottom = 2)]
		[SerializeField]
		[LabelText("資源加載器")]
		private BundleLoader _bundleLoader;

		[HorizontalGroup("Bundle Downloader/1/9", MarginLeft = 10, MarginRight = 10, LabelWidth = 100)]
		[VerticalGroup("Bundle Downloader/1/9/1", PaddingTop = 10, PaddingBottom = 2)]
		[SerializeField]
		[LabelText("主要資源完成事件")]
		private UnityEvent MainAssetDownloadSuccessEvent;

		[SerializeField]
		private Transform StageList;
		[SerializeField]
		private Transform GoStageBtn;

		private AsyncOperationHandle<List<string>> CatalogHandle;

		private List<string> Result = new List<string>();

		private AsyncOperationHandle<long> AssetSize;

		private AsyncOperationHandle DownloadHandle;

		private List<AssetReference> _assetReference;

		private bool ConfirmDownload = false;

		private float _downloadPercent;

		private float _downloadProgress;

		private bool _downloadDone;

		public static BundleDownloader Instance;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			_popText.InitialPop();

			CheckUpdateFiles();
		}

		/// <summary>
		/// 下載主要資源
		/// </summary>
		public void DownloadMainAsset()
		{
			ConfirmDownload = true;
			MXMDotween.instance.Fade(DownloadFrameCG, false, 0.5f);
			MXMDotween.instance.Scale(DownloadFrameRect, false, 0.5f, Ease.InBack);
		}

		/// <summary>
		/// 離開
		/// </summary>
		public void Exit()
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
		}

		public void GenerateSubList()
		{
			GenerateSubListAsync();
		}

		/// <summary>
		/// 下載次要資源
		/// </summary>
		/// <param name="_key"></param>
		public void DownloadSubAsset(string _key)
		{
			DownloadSubAssetAsync(_key);
		}

		/// <summary>
		/// 下載次要資源(Async)
		/// </summary>
		private async UniTask DownloadSubAssetAsync(string _key)
		{
			for (int i = 0; i < _subAssetList.Count; i++)
			{
				if (_subAssetList[i].SubAssetName.Equals(_key))
				{
					// 檢查是否有尚未下載的次要資源
					for (int j = 0; j < _subAssetList[i]._subAssetReference.Count; j++)
						await CheckDownloadSizeAsync(_subAssetList[i]._subAssetReference[j]);
				}
			}
			// 開始加載資源
			_bundleLoader.LoadStage(_key);
		}

		private async UniTask GenerateSubListAsync()
		{
			//await GetSubList();

			for (int i = 0; i < _subAssetList.Count; i++)
			{
				GameObject _gb = Instantiate(GoStageBtn, StageList).gameObject;
				_gb.transform.GetChild(0).GetComponent<Text>().text = string.Concat("關卡 ",i);
				int _sort = i;
				_gb.GetComponent<Button>().onClick.AddListener(() =>
				{
					DownloadSubAsset(_subAssetList.ElementAt(_sort).SubAssetName);
				});
			}
		}

		//private async UniTask GetSubList()
		//{
		//	_subAssetList = await Addressables.LoadAssetAsync<SubAssetList>(SubAssetListPath);
		//}

		/// <summary>
		/// 檢查是否有更新檔
		/// </summary>
		private async UniTask CheckUpdateFiles()
		{

			await ShowMsg("初始化檢查更新");

			await Addressables.InitializeAsync().ToUniTask();

			await ShowMsg("初始化檢查更新完成");

			CatalogHandle = Addressables.CheckForCatalogUpdates(false);

			await CatalogHandle.ToUniTask();

			if (CatalogHandle.Status == AsyncOperationStatus.Succeeded)
			{
				Result = CatalogHandle.Result;

				if (Result.Count > 0)
					await Addressables.UpdateCatalogs(Result).ToUniTask();

				// 檢查是否有尚未下載的主要資源
				for (int i = 0; i < _mainAssetReference.Count; i++)
					await CheckDownloadSizeAsync(_mainAssetReference[i]);

				#region 【取得所有尚未下載的資源大小】
				// 取得所有尚未下載的資源大小
				// foreach (var locator in Addressables.ResourceLocators)
				//	 foreach (var key in locator.Keys)
				//		 await CheckDownloadSizeAsync(key);
				#endregion

				Addressables.Release(CatalogHandle);

				await ShowMsg("主要資源驗證完成");

				MainAssetDownloadSuccessEvent.Invoke();
			}
			else
			{
				await ShowMsg("主要資源驗證失敗");
			}
		}

		/// <summary>
		/// 檢查資源大小
		/// </summary>
		private async UniTask CheckDownloadSizeAsync(object _key)
		{
			AssetSize = Addressables.GetDownloadSizeAsync(_key);

			await AssetSize.ToUniTask();

			if (AssetSize.Result > 0)
			{
				ConfirmDownload = false;
				await ShowMsg("資源名稱 : " + _key.ToString());
				await ShowMsg("資源大小 : " + AssetSize.Result);
				if (!ConfirmDownload)
				{
					MXMDotween.instance.Fade(DownloadFrameCG, true, 0.5f);
					MXMDotween.instance.Scale(DownloadFrameRect, true, 0.5f, Ease.OutBack);
					await UniTask.WaitUntil(() => ConfirmDownload == true);
				}
				await DownloadAsync(_key);
			}

			Addressables.Release(AssetSize);
		}

		/// <summary>
		/// 下載資源
		/// </summary>
		private async UniTask DownloadAsync(object _key)
		{
			await ShowMsg("開始下載");
			await ShowMsg("下載進度 : 0%");
			DownloadHandle = Addressables.DownloadDependenciesAsync(_key, false);

			_downloadPercent = 0;
			_downloadProgress = 0;
			_downloadDone = false;

			while (!_downloadDone)
			{
				_downloadPercent = DownloadHandle.GetDownloadStatus().Percent;
				while (_downloadProgress < _downloadPercent)
				{
					_downloadProgress += Time.deltaTime;
					if (_downloadProgress >= 1f)
					{
						_downloadProgress = 1f;
						_downloadDone = true;
					}
					await ShowMsg(string.Concat("下載進度 : ", (_downloadProgress * 100).ToString("0.0"), "%"), true);
					await UniTask.Yield();
				}
				await UniTask.Yield();
			}

			if (DownloadHandle.Status == AsyncOperationStatus.Succeeded)
			{
				Addressables.Release(DownloadHandle);
				await ShowMsg("下載完成");
			}
			else
			{
				await ShowMsg("下載失敗");
			}
		}

		/// <summary>
		/// 資源提示
		/// </summary>
		private async UniTask ShowMsg(string _msg, bool _stayText = false)
		{
			if (!IsShowMsg) return;
			Debug.Log(_msg);
			await _popText.Pop(_msg, _stayText);
		}
	}
}
