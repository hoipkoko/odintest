using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;

namespace MXM_Script
{
	public class PopText : MonoBehaviour
	{
		[SerializeField]
		[LabelText("Pop數量")]
		[Range(1f, 10f)]
		private int PopCount = 2;

		[SerializeField]
		[LabelText("Pop距離")]
		private float PopDistance = 60;

		[SerializeField]
		[LabelText("Pop時間")]
		[Range(0.1f, 5f)]
		private float PopTime = 0.5f;


		[SerializeField]
		[LabelText("Pop顏色")]
		private Color PopColor = Color.black;

		private Transform ContentText;

		private Text[] TextGroup;

		private RectTransform[] TextRect;

		private float ScaleRate;

		Sequence PopTween;

		public void InitialPop()
		{
			PopCount += 1;

			ScaleRate = 1f / PopCount;

			ContentText = transform.GetChild(0);
			for (int i = 0; i < PopCount - 1; i++)
				Instantiate(ContentText, transform);

			TextGroup = new Text[PopCount];
			TextRect = new RectTransform[PopCount];

			for (int i = 0; i < PopCount; i++)
				TextGroup[i] = transform.GetChild(i).GetComponent<Text>();
			for (int i = 0; i < PopCount; i++)
				TextRect[i] = transform.GetChild(i).GetComponent<RectTransform>();
			TextGroup[0].color = new Color(PopColor.r, PopColor.g, PopColor.b, 1);
			for (int i = 1; i < PopCount - 1; i++)
				TextGroup[i].color = new Color(PopColor.r, PopColor.g, PopColor.b, 0.15f);
			TextGroup[PopCount - 1].color = new Color(PopColor.r, PopColor.g, PopColor.b, 0);
		}


		/// <summary>
		/// Pop文字
		/// </summary>
		public async UniTask Pop(string _text, bool _stayText = false)
		{
			if (!_stayText)
			{
				PopTween.Pause();
				PopTween.Kill();
				PopTween = DOTween.Sequence();

				for (int i = 0; i < PopCount; i++)
				{
					PopTween.Join(TextRect[i].DOAnchorPosY(PopSpacing(i, true), PopTime));
					PopTween.Join(TextRect[i].DOScale(PopScale(i, true), PopTime).SetEase(Ease.OutBack));
				}

				for (int i = PopCount - 1; i > 0; i--)
				{
					TextGroup[i].text = TextGroup[i - 1].text;
				}

				for (int i = 0; i < PopCount; i++)
				{
					TextRect[i].anchoredPosition = new Vector2(0, PopSpacing(i, false));
					TextRect[i].localScale = Vector3.one * PopScale(i, false);
				}
			}

			TextGroup[0].text = _text;

			if (!_stayText)
				await UniTask.Delay((int)(PopTime * 1000f));
		}

		/// <summary>
		/// Pop間距
		/// </summary>
		float PopSpacing(int _index, bool _Start)
		{
			return _Start ?
				(_index == 0) ?
				0 : 0 - _index * PopDistance
				: (_index == 0) ?
				PopDistance : PopDistance - _index * PopDistance;
		}

		/// <summary>
		/// Pop縮放
		/// </summary>
		float PopScale(int _index, bool _Start)
		{
			return _Start ?
				(_index == 0) ?
				1 : (_index == PopCount - 1) ?
				0f : 1 - _index * ScaleRate
				: (_index == 0) ? 0 : 1 - (_index - 1) * ScaleRate;
		}
	}
}